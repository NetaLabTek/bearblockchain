# Copyright 2019 endes
# Copyright 2017 Yoshihiro Tanaka
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at

  # http://www.apache.org/licenses/LICENSE-2.0

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Author: Yoshihiro Tanaka <contact@cordea.jp>
# date  : 2018-02-03

import tables
import math
import options

import bblock

type
  GetBlockProc*[T] = proc (chain: int, hash: string): Option[Block[T]]
  CanReplaceProc*[T] = proc (chain: int, new: Block[T], old: Block[T]): bool
  OnBlockProc*[T] = proc (chain: int, blk: Block[T], latest: bool)
  IsValidProc*[T] = proc (chain: int, blk: Block[T], a_block: Block[T]): bool

  Chain*[T] = object
    blocks*: Table[string, Block[T]]
    max_blocks*: int
    index: seq[string]
  BlockMHandlers*[T] = object
    getBlock: GetBlockProc
    canReplace: CanReplaceProc
    onBlock: OnBlockProc
    isValid: IsValidProc

  BlockManager*[T] = object
    n*: int
    chains*: Table[int, Chain[T]]
    handlers*: BlockMHandlers[T]

proc newBlockManager*[T](genesis_data: T, chains: int, max_blocks: seq[int]): BlockManager[T] =
  result = BlockManager[T](
    n: chains,
    chains: initTable(nextPowerOfTwo(chains)),
    handlers: BlockMHandlers()
  )

  for n in 0..chains:
    result.chains[n] = Chain(blocks: initTable(nextPowerOfTwo(max_blocks[n])), max_blocks: max_blocks[n])
  
  let genesis = getGenesisBlock(genesis_data)
  result.chains[0].blocks[genesis.hash] = genesis

proc latestBlock*[T](manager: BlockManager[T], chain: int = -1): Block[T] =
  var l_block: Block[T]

  if chain < 0:
    for chain in manager.chains:
      var c_block = chain.blocks[chain.index[chain.index.len() - 1]]
      if isNil(l_block) or c_block.depth > l_block.depth:
        l_block = c_block
  else:
    #TODO: Error
    if manager.chains[chain].len() == 0: return nil
    l_block = manager.chains[chain].blocks[manager.chains[chain].index[chain.index.len() - 1]]
  
  return l_block

proc getBlock*[T](manager: BlockManager[T], chain: int, hash: string): Option[Block[T]] =
  if not manager.chains[chain].blocks.hasKey(hash):
    return manager.handlers.getBlock(chain, hash)
  else:
    return manager.chains[chain].blocks[hash]

#TODO: use option instead of nil
proc isValid[T](manager: BlockManager[T], chain: int, blk: Block[T], a_block: Block[T]=nil): bool =
  if blk.calcHash() != blk.hash:
    return false

  if isNil(a_block):
    var a = manager.getBlock(chain, manager.chains[chain].index[blk.depth-1])
    if not a.isSome() or blk.previousHashs[chain] == a.get().hash:
      return manager.handlers.isValid(chain, blk, a)
    else:
      return false
  else:
    if blk.depth-1 == a_block.depth and blk.previousHashs[chain] == a_block.hash:
      return manager.handlers.isValid(chain, blk, a_block)
    else:
      return false

proc add*[T](manager: var BlockManager[T], chain: int, blk: Block[T]) =
  if isValid(manager, chain, blk, manager.latestBlock()):
    manager.saveBlock(chain, blk)
  else:
    #TODO: Throw error
    discard


proc isContinuos*[T](index: seq[string], blks: Table[string, Block[T]]): bool =
  var a_i: string
  for i in index:
    if a_i != "":
      if not isOrdered(blks[i], blks[a_i]):
        return false
    a_i = i
  return true

#TODO: use option instead of nil
proc canReplace[T](manager: BlockManager[T], chain: int, new: Block[T], original: Block[T]): bool =
  if isNil(new) or isNil(original):
    return false
  elif not isValid(manager, chain, original):
    return true
  elif isValid(manager, chain, new):
    return manager.handlers.canReplace(chain, new, original)
  else:
    return false

template getIndex[T](manager: BlockManager[T], chain: int, blk: Block[T]): int =
  for i in 0..manager.chains[chain].index.len():
    if manager.chains[chain].index[i] == blk.hash:
      return i

template saveBlock[T](manager: BlockManager[T], chain: int, blk: Block[T], latest=false) =
  manager.handlers.onBlock(chain, blk, latest)
  if isNil(manager.getIndex(chain, blk)):
    manager.chains[chain].index.add(blk.hash)
  manager.chains[chain].blocks[blk.hash] = blk


proc mergeIndex*[T](manager: BlockManager[T], chain: int, i: seq[string]) =
  if isNil(manager.chains[chain].index):
    manager.chains[chain].index = i
  elif manager.chains[chain].index != i:
    var o_i = manager.chains[chain].index
    for n in 0..i.len()-1:
      if isNil(o_i[n]):
        o_i[n] = i[n]
      elif o_i[n] != i[n]:
        if manager.canReplace(chain, manager.getBlock(chain, i[n]), manager.getBlock(chain, o_i[n])):
          o_i[n] = i[n]