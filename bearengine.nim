import rdstdin

import marshal
import bblock
import blockmanager
import message
import messagehandler

type
  Bearengine*[T] = object
    blocks: BlockManager[T]
    #msgs: 


proc newBearengine*[T](genesis_data: T, chains: int, max_blocks: seq[int]): Bearengine[T] =
  #result = newPeerhandler(genesis_data, chains, max_blocks, onMessage, onOpen)
  return Bearengine[T](
          blocks: newBlockManager(genesis_data, chains, max_blocks)
          #TODO
          #comm: newCommManager(onMessage, onOpen)
        )

template ´GetBlock=´*[T](handler: var Bearengine[T], p: GetBlockProc) =
  handler.blocks.handlers.getBlock = p

template ´OnBlock=´*[T](handler: var Bearengine[T], p: OnBlockProc) =
  handler.blocks.handlers.onBlock = p

template ´CanReplace=´*[T](handler: var Bearengine[T], p: CanReplaceProc) =
  handler.blocks.handlers.canReplace = p

template ´IsValid=´*[T](handler: var Bearengine[T], p: IsValidProc) =
  handler.blocks.handlers.isValid = p


proc generateBlock*[T](handler: var Bearengine[T], chain: int, multi_chain: seq[int], data: T): Block[T] =
  let blk = handler.blocks.latestBlock(chain)
  var p_hash = {chain: blk.hash}.table
  if not isNil(multi_chain):
    for c in multi_chain:
      p_hash[c] = handler.blocks.latestBlock(c).hash

  let newBlk = newBlock(
    blk.depth + 1,
    data,
    p_hash
  )
  #TODO: Broadcast via comm
  handler.blocks.add(chain, newBlk)
  return newBlk


#TODO: throw errors 
proc replaceBlock*[T](handler: var Bearengine[T], chain: int, index: seq[string], blks: Table[string, Block[T]]) =
  let blk = handler.blocks.latestBlock(chain)
  if isNil(blk): handler.blocks.chains[chain] = blks
    
  if isContinuos(index, blks):
    if blk.depth < blks[index[index.len() - 1]].depth:
      handler.blocks.chains[chain] = blks

#TODO: This on block manager
proc setIndex*[T](handler: var Bearengine[T], chain: int, index: seq[string]) =
  handler.blocks.chains[chain].index = index

proc obtainIndex*[T](handler: var Bearengine[T]) =
  discard
  #TODO: make request

#TODO: This on comm manager
#[proc getNodes*[T](handler: var Peerhandler[T], peer: Peer, far: bool=false) =
  if dht.len() < maxNodes:
    let response = newMessageBuilder[T]()
      .code(if far: GetFarNodes else: GetCloseNodes)
      .DHT_hash(handler.hash)
      .build()
      .toMsgpack()
    peer.send(response)]#


proc initBearengine*[T](handler: var Bearengine[T]) =
  if handler.blocks.handlers.getBlock == nil:
    #TODO
    #handler.GetBlock = 
    discard
  if handler.blocks.handlers.canReplace == nil:
    #TODO
    #handler.CanReplace = 
    discard
  if handler.blocks.handlers.onBlock == nil:
    #TODO
    #handler.OnBlock = 
    discard
  if handler.blocks.handlers.isValid == nil:
    #TODO
    #handler.IsValid = 
    discard

  handler.msgs.init()
  handler.blocks.init()