import bblock
import message
import messages
import blockmanager
import commanager

import asyncdispatch
import asyncfutures
import tables

type
  DHT = Table[int64, tuple[distance: int64, reqs: int, bad: int]]

  MessageHandler = object
    dht: DHT
    comm: ComManager 
    config: tuple[
      maxNodes,
      maxBadResponses,
      timeoutms,
      zeroBadRequestms: int
    ]

  Timeout = object of Exception

#TODO: changeable
const
  maxNodes* = 100
  maxBadResponses = 5
  zeroBadRequestms = 2500

template send_request*[T](handler: MessageHandler, peerHash: int64, data: Message[T]): Future[Message[T]] =
  var requestFut: Future[Message[T]] = newFuture("send_request")
  let fut = handler.comm.send[peerHash, data.toMsgpack()]

  withTimeout(fut, handler.config.timeoutms).callback = proc(t: Future[bool]) =
    if not t.value:
      handler.dht[peerHash].bad += 1
      fut.fail(newException(Timeout, "Timeouted Request"))

  fut.addCallback(proc(t: Future[string]) = 
    if t.failed:
      requestFut.fail(t.readError())
    else:
      requestFut.complete(t.read().fromMsgpack())
  )

  handler.dht[peerHash].reqs += 1  
  requestFut

proc hash_distance(h1: int64, h2: int64): int64 =
  return h1 xor h2

#TODO: macro
#processMsg getNodes, GetFarNodes, GetCloseNodes:
# onRequest:
#   code
# onRenspose:
#   check SetNodes, dht
#   code
macro processMsg(msg: untyped): return type =
  discard
  