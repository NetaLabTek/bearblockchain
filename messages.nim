import message

proc getNodes*[T](nodes: seq[DHTNode], far=false): string =
    result = build(if far: GetFarNodes else: GetCloseNodes):
        DHT_nodes = nodes

proc getNodesResponse*[T](nodes: seq[DHTNode]): string =
    result = build(SetNodes):
        DHT_nodes = nodes

proc requestConnection*[T](hash: string, params: string): string =
    result = build(RequestCon):
        DHT_hash = hash
        DHT_nodes = @[[hash: 0, distance: 0, params: params]]

proc requestParams*[T](hash: string): string =
    result = build(RequestParms):
        DHT_hash = hash

proc requestParamsResponse*[T](node: tuple[hash: int, distance: int, params: string]): string =
    result = build(ResponseParms):
        DHT_nodes = @[node]

proc getIndexResponse*[T](chain: int, index: seq[string]): string =
    result = build(SetIndex):
        chain = chain
        index = index

proc getRequest*[T](hash: int): string =
    result = build(Request):
        block_hash = hash

#[proc getResponse*[T](handler: BlockManager[T], chain: int, hash: int): string =
    result = build(Response):
        chain = chain
        blocks = @[handler.manager.getBlock(chain, hash)]

proc getAllRequest*[T](): string =
    result = build(RequestAll):
        discard

proc getAllResponse*[T](handler: var BlockManager[T]): string =
    result = build(ResponseAll):
        blocks = handler.manager.blocks

proc getLatestRequest*[T](): string =
    result = build(RequestLatest):
        discard

proc getLatestResponse*[T](handler: var BlockManager[T]): string =
    result = build(RequestLatest):
        blocks = @[handler.manager.latestBlock()]]#
