[Package]
name          = "bearengine"
version       = "0.1.0"
author        = "endes"
description   = "Blockchain engine"
license       = "Apache 2.0"

[Deps]
Requires: "dbg >= 0.1.1"
Requires: "cascade >= 0.2.0"
Requires: "interfaced >= 0.2.0"
Requires: "nimSHA2 >= 0.1.1"
Requires: "msgpack4nim >= 0.2.3"
