# Copyright 2019 endes
# Copyright 2017 Yoshihiro Tanaka
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at

  # http://www.apache.org/licenses/LICENSE-2.0

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Author: Yoshihiro Tanaka <contact@cordea.jp>
# date  : 2018-02-04

import bblock

import options
import marshal

import cascade
import dbg

#TODO: changable
const maxPeers = 33

type
  Code* = enum
    GetFarNodes,
    GetCloseNodes,
    SetNodes,
    RequestCon,
    RequestParms,
    ResponseParms,

    GetIndex,
    SetIndex,

    Request,
    Response,
    RequestAll,
    ResponseAll,
    RequestLatest,
    ResponseLatest

  DHTNode* = tuple[hash: int64, distance: int64]

  DHTData* = object
    hash*: int
    maxpeers*: range[1..maxPeers]
    nodes: seq[DHTNode]

  Message*[T] = object
    code*: Code
    hash*: Option[int]
    chain*: Option[int] 
    index*: Option[seq[string]]
    blocks*: Option[seq[Block[T]]]
    dht*: Option[DHTData]


#TODO: Option
proc newMessage*[T](): Message[T] =
  result = Message[T](
    hash: none(int),
    chain: none(int),
    blocks: @[],
    index: @[],
    dht: none(DHTData)
  )

proc `chain=`*[T](builder: var Message[T], chain: int) =
  builder.chain = some(chain)

proc `block_hash=`*[T](builder: var Message[T], hash: int) =
  builder.hash = some(hash)

template DHTCheck[T](builder: Message[T]) =
  if not builder.dht.isSome:
    builder.dht = DHTData()

proc `DHT_nodes=`*[T](builder: var Message[T], nodes: seq[tuple[hash: int, distance: int, params: string]]) =
  builder.DHTCheck()
  builder.dht.nodes = nodes

proc `DHT_hash=`*[T](builder: var Message[T], hash: string) =
  builder.DHTCheck()
  builder.dht.hash = hash

#TODO: incorrect
proc toJson*[T](message: Message[T]): string =
  result = $$message

proc toMsgpack*[T](message: Message[T]): string =
  result = $$message#pack(message)

proc fromMsgpack*[T](message: string): Message[T]=
  result = to(message)#unpack(message)

template build*[T](msgCode: Code, assgn: untyped): string =
  let msg = cascade newMessageBuilder[T]():
    code = msgCode
    assgn

  dbg "Builded msg: " & $msgCode & " " & $msg
  msg.toMsgpack()