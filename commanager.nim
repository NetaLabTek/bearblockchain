import interfaced, asyncfutures
import message

createInterface(*ComManager):
    proc send*(this: ComManager, hash: int64, msg: string): Future[string]
    proc broadcast*(this: ComManager, msg: string)
    proc disconnect*(this: ComManager, hash: int64)
        

    proc myHash*(this: ComManager): int64
    proc myParams*(this: ComManager): string
        
    proc onMsg*(this: ComManager, cb: proc(hash: int64, msg: string))
    proc onDisconnect*(this: ComManager, cb: proc(hash: int64))
    proc onConnect*(this: ComManager, cb: proc(hash: int64))